# zakharov/cron-validators

## Примеры
### Проверка по частям:
```php
(new \Zakharov\CronValidators\MinutesFieldValidator())->isValid('0-50/5');
(new \Zakharov\CronValidators\HoursFieldValidator())->isValid('*');
(new \Zakharov\CronValidators\DayOfMonthFieldValidator())->isValid('31');
(new \Zakharov\CronValidators\MonthFieldValidator())->isValid('JAN');
(new \Zakharov\CronValidators\DayOfWeekFieldValidator())->isValid('SUN');
```
### Проверка выражений
```php
(new \Zakharov\CronValidators\CronExpressionValidator())->isValid('* * * * *');
```

### Проверка строки crontab
```php
(new \Zakharov\CronValidators\LineValidator())->isValid('* * * * * echo 1 >> /logs/test.log');
```
