<?php declare(strict_types=1);

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\MinutesFieldValidator;

class MinutesFieldValidatorTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new MinutesFieldValidator();

        $this->assertTrue($validator->isValid('*'));
        $this->assertTrue($validator->isValid('30'));
        $this->assertTrue($validator->isValid('0'));
        $this->assertTrue($validator->isValid('00'));
        $this->assertTrue($validator->isValid('01'));
        $this->assertTrue($validator->isValid('59'));
        $this->assertTrue($validator->isValid('*/5'));
        $this->assertTrue($validator->isValid('59'));
        $this->assertTrue($validator->isValid('1,2'));
        $this->assertTrue($validator->isValid('*/3,1,1-12'));
        $this->assertTrue($validator->isValid('40-10,20-30/3'));

        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('text'));
        $this->assertFalse($validator->isValid('1-2-3'));
        $this->assertFalse($validator->isValid('*-3'));
        $this->assertFalse($validator->isValid('-3'));
        $this->assertFalse($validator->isValid('60'));
        $this->assertFalse($validator->isValid('70'));
        $this->assertFalse($validator->isValid('* / 5'));
        $this->assertFalse($validator->isValid('1.2'));
        $this->assertFalse($validator->isValid('1/2'));
    }

}
