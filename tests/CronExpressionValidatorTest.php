<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.03.2023
 * @copyright {Template_Description_Copyrights}
 */

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\CronExpressionValidator;

class CronExpressionValidatorTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new CronExpressionValidator();

        $this->assertTrue($validator->isValid('* * * * *'));
        $this->assertTrue($validator->isValid('  * * * * *'));
        $this->assertTrue($validator->isValid('  * * * * *  '));
        $this->assertTrue($validator->isValid('* * * * *  '));
        $this->assertTrue($validator->isValid('*	*	*	*	*'));
        $this->assertTrue($validator->isValid('*	 *	 *		*		*'));
        $this->assertTrue($validator->isValid('*/5   1,2,3   31   *   SUN'));

        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('* * * * * *'));
        $this->assertFalse($validator->isValid('* * * *'));
        $this->assertFalse($validator->isValid('1/4 * * * *'));
        $this->assertFalse($validator->isValid('60 * * * *'));
        $this->assertFalse($validator->isValid('* 25 * * *'));
        $this->assertFalse($validator->isValid('* * 40 * *'));
        $this->assertFalse($validator->isValid('* * * 13 *'));
        $this->assertFalse($validator->isValid('* * * * 8'));
    }
}
