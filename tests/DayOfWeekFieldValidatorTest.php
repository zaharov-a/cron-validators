<?php declare(strict_types=1);

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\DayOfWeekFieldValidator;

class DayOfWeekFieldValidatorTest extends TestCase
{

    public function testIsValid()
    {
        $validator = new DayOfWeekFieldValidator();

        $this->assertTrue($validator->isValid('*'));
        $this->assertTrue($validator->isValid('7'));
        $this->assertTrue($validator->isValid('1'));
        $this->assertTrue($validator->isValid('01'));
        $this->assertTrue($validator->isValid('*/5'));
        $this->assertTrue($validator->isValid('6'));
        $this->assertTrue($validator->isValid('1,2'));
        $this->assertTrue($validator->isValid('*/3,1,1-7'));
        $this->assertTrue($validator->isValid('7-5,2-4/3'));
        $this->assertTrue($validator->isValid('sun'));
        $this->assertTrue($validator->isValid('SUN'));
        $this->assertTrue($validator->isValid('SUN-2'));

        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('text'));
        $this->assertFalse($validator->isValid('1-2-3'));
        $this->assertFalse($validator->isValid('*-3'));
        $this->assertFalse($validator->isValid('-3'));
        $this->assertFalse($validator->isValid('60'));
        $this->assertFalse($validator->isValid('70'));
        $this->assertFalse($validator->isValid('* / 5'));
        $this->assertFalse($validator->isValid('1.2'));
        $this->assertFalse($validator->isValid('1/2'));
    }
}
