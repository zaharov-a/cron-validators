<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.03.2023
 * @copyright {Template_Description_Copyrights}
 */

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\LineValidator;

class LineValidatorTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new LineValidator();

        $this->assertTrue($validator->isValid('* * * * * *'));
        $this->assertTrue($validator->isValid(' * * * * * * '));
        $this->assertTrue($validator->isValid('	 * * * * * *	 	 '));
        $this->assertTrue($validator->isValid('* * * * * echo 1 >> /logs/test.log'));
        $this->assertTrue($validator->isValid('*	*	*	*	* echo 1 >> /logs/test.log'));
        $this->assertTrue($validator->isValid('*	 *	 *		*		* echo 1 >> /logs/test.log'));
        $this->assertTrue($validator->isValid('*/5   1,2,3   31   *   SUN echo 1 >> /logs/test.log'));

        $this->assertFalse($validator->isValid('* * * * *  '));
        $this->assertFalse($validator->isValid('  * * * * *  '));
        $this->assertFalse($validator->isValid('  * * * * *'));
        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('* * * *'));
        $this->assertFalse($validator->isValid('1/4 * * * * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('* / 4 * * * * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('60 * * * * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('* 25 * * * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('* * 40 * * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('* * * 13 * echo 1 >> /logs/test.log'));
        $this->assertFalse($validator->isValid('* * * * 8  echo 1 >> /logs/test.log'));
    }
}
