<?php declare(strict_types=1);

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\MonthFieldValidator;

class MonthFieldValidatorTest extends TestCase
{
    public function testIsValid()
    {
        $validator = new MonthFieldValidator();

        $this->assertTrue($validator->isValid('*'));
        $this->assertTrue($validator->isValid('12'));
        $this->assertTrue($validator->isValid('1'));
        $this->assertTrue($validator->isValid('01'));
        $this->assertTrue($validator->isValid('*/5'));
        $this->assertTrue($validator->isValid('6'));
        $this->assertTrue($validator->isValid('1,2'));
        $this->assertTrue($validator->isValid('*/3,1,1-12'));
        $this->assertTrue($validator->isValid('11-10,9-10/3'));
        $this->assertTrue($validator->isValid('jan'));
        $this->assertTrue($validator->isValid('JAN'));
        $this->assertTrue($validator->isValid('JAN-2'));

        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('text'));
        $this->assertFalse($validator->isValid('1-2-3'));
        $this->assertFalse($validator->isValid('*-3'));
        $this->assertFalse($validator->isValid('-3'));
        $this->assertFalse($validator->isValid('60'));
        $this->assertFalse($validator->isValid('70'));
        $this->assertFalse($validator->isValid('* / 5'));
        $this->assertFalse($validator->isValid('1.2'));
        $this->assertFalse($validator->isValid('1/2'));
    }
}
