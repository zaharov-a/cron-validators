<?php declare(strict_types=1);

namespace Zakharov\CronValidators\Tests;

use PHPUnit\Framework\TestCase;
use Zakharov\CronValidators\HoursFieldValidator;

class HoursFieldValidatorTest extends TestCase
{

    public function testIsValid()
    {
        $validator = new HoursFieldValidator();

        $this->assertTrue($validator->isValid('*'));
        $this->assertTrue($validator->isValid('12'));
        $this->assertTrue($validator->isValid('0'));
        $this->assertTrue($validator->isValid('00'));
        $this->assertTrue($validator->isValid('01'));
        $this->assertTrue($validator->isValid('23'));
        $this->assertTrue($validator->isValid('*/5'));
        $this->assertTrue($validator->isValid('1,2'));
        $this->assertTrue($validator->isValid('*/3,1,1-12'));
        $this->assertTrue($validator->isValid('9-10,20-10/3'));

        $this->assertFalse($validator->isValid(''));
        $this->assertFalse($validator->isValid('text'));
        $this->assertFalse($validator->isValid('1-2-3'));
        $this->assertFalse($validator->isValid('*-3'));
        $this->assertFalse($validator->isValid('-3'));
        $this->assertFalse($validator->isValid('31'));
        $this->assertFalse($validator->isValid('70'));
        $this->assertFalse($validator->isValid('* / 5'));
        $this->assertFalse($validator->isValid('1.2'));
        $this->assertFalse($validator->isValid('1/2'));
    }

}
