<?php declare(strict_types=1);

namespace Zakharov\CronValidators;

class DayOfWeekFieldValidator extends AbstractFieldValidator
{
    protected $range = [0, 7];

    protected $aliases = [
        'MON' => 1,
        'TUE' => 2,
        'WED' => 3,
        'THU' => 4,
        'FRI' => 5,
        'SAT' => 6,
        'SUN' => 7,
    ];
}

