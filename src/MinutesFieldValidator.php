<?php declare(strict_types=1);

namespace Zakharov\CronValidators;

class MinutesFieldValidator extends AbstractFieldValidator
{
    protected $range = [0, 59];
}

