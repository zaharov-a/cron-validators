<?php declare(strict_types=1);

namespace Zakharov\CronValidators;

class DayOfMonthFieldValidator extends AbstractFieldValidator
{
    protected $range = [1, 31];
}
