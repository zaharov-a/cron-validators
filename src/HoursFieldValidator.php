<?php declare(strict_types=1);

namespace Zakharov\CronValidators;

class HoursFieldValidator extends AbstractFieldValidator
{
    protected $range = [0, 23];
}

