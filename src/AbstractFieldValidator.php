<?php declare(strict_types=1);

namespace Zakharov\CronValidators;

/**
 * @see https://en.wikipedia.org/wiki/Cron#Cron_expression
 */
class AbstractFieldValidator
{
    /**
     * @var int[]
     */
    protected $range;

    /**
     * @var array<int|string>
     */
    protected $aliases;

    public function isValid(string $value): bool
    {
        if ($this->isList($value)) {
            return $this->isValidList($value);
        }

        return $this->isValidItem($value);
    }

    private function isAll(string $value): bool
    {
        return $value === '*';
    }

    private function isList(string $value): bool
    {
        return strpos($value, ',') !== false;
    }

    private function isValidList(string $value): bool
    {
        $parts = explode(',', $value);
        foreach ($parts as $part) {
            if (!$this->isValidItem($part)) {
                return false;
            }
        }

        return true;
    }

    private function isValidItem(string $value): bool
    {
        if ($this->isAll($value)) {
            return true;
        }

        if ($this->isStep($value)) {
            return $this->isValidStep($value);
        }

        if ($this->isRange($value)) {
            return $this->isValidRange($value);
        }

        if ($this->aliases) {
            $value = $this->convertAlias($value);
        }

        if ($this->isInt($value)) {
            return $this->inRange($value);
        }

        return false;
    }

    private function isStep(string $value): bool
    {
        return strpos($value, '/') !== false;
    }

    private function isValidStep(string $value): bool
    {
        [$range, $step] = explode('/', $value);

        if (is_numeric($range)) {
            return false;
        }

        return $this->isValidItem($range) && filter_var($step, FILTER_VALIDATE_INT);
    }

    private function isRange(string $value): bool
    {
        return strpos($value, '-') !== false;
    }

    private function isValidRange(string $value): bool
    {
        $parts = explode('-', $value);
        if (count($parts) > 2) {
            return false;
        }

        [$left, $right] = $parts;

        if ($this->aliases) {
            $left  = $this->convertAlias($left);
            $right = $this->convertAlias($right);
        }

        return $this->isInt($left) && $this->isInt($right) &&
            $this->inRange($left) && $this->inRange($right);
    }

    private function isInt(string $value): bool
    {
        return is_numeric($value) && strpos($value, '.') == false;
    }

    private function inRange(string $value): bool
    {
        $i = (int)$value;

        return $this->range[0] <= $i && $i <= $this->range[1];
    }

    private function convertAlias(string $value): string
    {
        return strval($this->aliases[strtoupper($value)] ?? $value);
    }
}
