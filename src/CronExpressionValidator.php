<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.03.2023
 * @copyright {Template_Description_Copyrights}
 */

namespace Zakharov\CronValidators;

class CronExpressionValidator
{

    const PART_COUNT = 5;

    /**
     * # ┌───────────── minute (0 - 59)
     * # │ ┌───────────── hour (0 - 23)
     * # │ │ ┌───────────── day of the month (1 - 31)
     * # │ │ │ ┌───────────── month (1 - 12)
     * # │ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday;
     * # │ │ │ │ │                                   7 is also Sunday on some systems)
     * # │ │ │ │ │
     * # │ │ │ │ │
     * # * * * * *
     * @param string $expression
     * @return bool
     */
    public function isValid(string $expression): bool
    {
        if (!($parts = $this->parse($expression))) {
            return false;
        }

        [$minutes, $hours, $dayOfMonth, $month, $dayOfWeek] = $parts;

        return
            (new MinutesFieldValidator())->isValid($minutes) &&
            (new HoursFieldValidator())->isValid($hours) &&
            (new DayOfMonthFieldValidator())->isValid($dayOfMonth) &&
            (new MonthFieldValidator())->isValid($month) &&
            (new DayOfWeekFieldValidator())->isValid($dayOfWeek);
    }

    private function parse(string $expression): ?array
    {
        $parts = preg_split("/(\s+)/", trim($expression), self::PART_COUNT);

        if (count($parts) != self::PART_COUNT) {
            return null;
        }

        return $parts;
    }
}
